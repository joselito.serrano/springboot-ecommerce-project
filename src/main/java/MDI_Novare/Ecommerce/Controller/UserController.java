package MDI_Novare.Ecommerce.Controller;

import MDI_Novare.Ecommerce.Repository.User;
import MDI_Novare.Ecommerce.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/add")
    public User addUser(@RequestBody User user) {
        return userService.save(user);
    }

    @GetMapping("/getAll")
    public List<User> getAllUser() {
        return userService.findAll();
    }

    @GetMapping("/getById")
    public User getById(@RequestParam long id) {
        return userService.findById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id) {
        userService.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public User updateById(@PathVariable long id, @RequestBody User user) {
        return userService.updateById(id, user);
    }

    //Additional
    @PostMapping("/getUser")
    public User getByUsername(@RequestBody User user){
        return userService.findByUsername(user.getUsername());
    }

}

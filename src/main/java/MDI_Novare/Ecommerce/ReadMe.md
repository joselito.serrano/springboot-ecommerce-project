Create an Ecommerce application for a store which sells different gadgets.

You're application must have the following features:

    Registration - Create user
        roles: buyer or seller

    Login / Logout feature

    Product catalog 1. add product 2. update product 3. delete product 4. view catalog

     - determine the feature which only a certain role can access

    Cart 1. add 2. update 3. delete 4. view

     - determine the feature which only a certain role can access

    Utilize Spring boot and its features we have discussed:
        Restful Web Service
        Thymeleaf / HTML/ CSS / JSP
        Spring JPA / Hibernate
        Spring Security


# Todo
name in form is what getting passed
validation

updateCrudProduct fix the delete,update,mapping
package MDI_Novare.Ecommerce.Service;

import MDI_Novare.Ecommerce.Repository.Cart;
import MDI_Novare.Ecommerce.Repository.User;

import java.util.List;

public interface CartService {
    Cart save(Cart cart);

    List<Cart> findAll();

    Cart findById(long id);

    void deleteById(long id);

    Cart updateById(long id, Cart cart);

    List<Cart> findByBuyer(User user);
}

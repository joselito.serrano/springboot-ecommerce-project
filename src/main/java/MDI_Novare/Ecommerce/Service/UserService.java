package MDI_Novare.Ecommerce.Service;

import MDI_Novare.Ecommerce.Repository.User;

import java.util.List;

public interface UserService {
    User save(User user);

    List<User> findAll();

    User findById(long id);

    void deleteById(long id);

    User updateById(long id, User user);

    User findByUsername(String userName);

}

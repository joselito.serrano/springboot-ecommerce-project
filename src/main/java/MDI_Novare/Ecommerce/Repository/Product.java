package MDI_Novare.Ecommerce.Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Entity
@Table
@Setter
@Getter
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String price;
    private int quantity;
    //Reference the user(seller)
    private String description;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_user_id"
            , referencedColumnName = "id"
            , foreignKey = @ForeignKey(name = "FK_User_Product"
            , value = ConstraintMode.PROVIDER_DEFAULT))
    private User seller;

    //To do find what this is for
//    @JsonIgnore
//    @OneToMany(mappedBy = "id")
//    private Set<Cart> cartSet;

}

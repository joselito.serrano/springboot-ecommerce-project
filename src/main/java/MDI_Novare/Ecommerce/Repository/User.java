package MDI_Novare.Ecommerce.Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Entity
@Table
@Setter
@Getter
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String role;

    //To remove
//    @JsonIgnore
//    @OneToMany(mappedBy = "buyer")
//    private Set<Cart> cartSet;
//
//    @JsonIgnore
//    @OneToMany(mappedBy = "seller")
//    private Set<Product> productSet;

    //To do find what this is for
//    @JsonIgnore
//    @OneToMany(mappedBy = "id")
//    private Set<Cart> cartSet;

}

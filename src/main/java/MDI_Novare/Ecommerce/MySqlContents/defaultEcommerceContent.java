package MDI_Novare.Ecommerce.MySqlContents;

import MDI_Novare.Ecommerce.Config.PasswordEncoder;
import MDI_Novare.Ecommerce.Repository.Cart;
import MDI_Novare.Ecommerce.Repository.Product;
import MDI_Novare.Ecommerce.Repository.User;
import MDI_Novare.Ecommerce.Service.CartService;
import MDI_Novare.Ecommerce.Service.ProductService;
import MDI_Novare.Ecommerce.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

@Component
public class defaultEcommerceContent implements CommandLineRunner {

    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CartService cartService;
    private final ArrayList<User> defaultUserData = new ArrayList<>();
    private final ArrayList<Product> defaultProductData = new ArrayList<>();

    @Override
    public void run(String... args) throws Exception {
        populateUserTable();
        populateProductTable();
        populateCartTable();
    }

    private void populateUserTable() throws NoSuchAlgorithmException {
        User user1 = new User();
        user1.setFirstName("Michael");
        user1.setLastName("Smith");
        user1.setEmail("michaelsmith@gmail.com");
        user1.setUsername("michaelsmith");
        user1.setRole("seller");
//        user1.setPassword("M!chaelSmiTh123");
        user1.setPassword(PasswordEncoder.encode("M!chaelSmiTh123"));
//        user1.setPassword(PasswordEncoder.encode("admin"));
        userService.save(user1);

        User user2 = new User();
        user2.setFirstName("Jessica");
        user2.setLastName("Johnson");
        user2.setEmail("jessicajohnson@hotmail.com");
        user2.setUsername("jessicajohnson");
        user2.setRole("seller");
//        user2.setPassword("J0hnsonJess1ca#");
        user2.setPassword(PasswordEncoder.encode("J0hnsonJess1ca#"));
        userService.save(user2);

        User user3 = new User();
        user3.setFirstName("William");
        user3.setLastName("Brown");
        user3.setEmail("williambrown@yahoo.com");
        user3.setUsername("williambrown");
        user3.setRole("seller");
//        user3.setPassword("BrownWilliam99@");
        user3.setPassword(PasswordEncoder.encode("BrownWilliam99@"));
        userService.save(user3);

        User user4 = new User();
        user4.setFirstName("Olivia");
        user4.setLastName("Davis");
        user4.setEmail("oliviadavis@outlook.com");
        user4.setUsername("oliviadavis");
        user4.setRole("seller");
//        user4.setPassword("Dav1sOl1v1a%");
        user4.setPassword(PasswordEncoder.encode("Dav1sOl1v1a%"));
        userService.save(user4);

        User user5 = new User();
        user5.setFirstName("Christopher");
        user5.setLastName("Garcia");
        user5.setEmail("christophergarcia@aol.com");
        user5.setUsername("christophergarcia");
        user5.setRole("seller");
//        user5.setPassword("GarciaChri5t0pher^");
        user5.setPassword(PasswordEncoder.encode("GarciaChri5t0pher^"));
        userService.save(user5);

        User user6 = new User();
        user6.setFirstName("Sophia");
        user6.setLastName("Rodriguez");
        user6.setEmail("sophiarodriguez@protonmail.com");
        user6.setUsername("sophiarodriguez");
        user6.setRole("buyer");
//        user6.setPassword("Rodrigue2Soph!a");
        user6.setPassword(PasswordEncoder.encode("Rodrigue2Soph!a"));
        userService.save(user6);

        User user7 = new User();
        user7.setFirstName("Daniel");
        user7.setLastName("Martinez");
        user7.setEmail("danielmartinez@icloud.com");
        user7.setUsername("danielmartinez");
        user7.setRole("buyer");
//        user7.setPassword("Mart1nezDan!el#");
        user7.setPassword(PasswordEncoder.encode("Mart1nezDan!el#"));
        userService.save(user7);

        User user8 = new User();
        user8.setFirstName("Emily");
        user8.setLastName("Gonzalez");
        user8.setEmail("emilygonzalez@zoho.com");
        user8.setUsername("emilygonzalez");
        user8.setRole("buyer");
//        user8.setPassword("Gonzal3zEm1ly$");
        user8.setPassword(PasswordEncoder.encode("Gonzal3zEm1ly$"));
        userService.save(user8);

        User user9 = new User();
        user9.setFirstName("Matthew");
        user9.setLastName("Wilson");
        user9.setEmail("matthewwilson@mail.com");
        user9.setUsername("matthewwilson");
        user9.setRole("buyer");
//        user9.setPassword("W!lsonMatthew123");
        user9.setPassword(PasswordEncoder.encode("W!lsonMatthew123"));
        user9.setPassword(PasswordEncoder.encode("123"));
        userService.save(user9);

        User user10 = new User();
        user10.setFirstName("Madison");
        user10.setLastName("Lopez");
        user10.setEmail("madisonlopez@gmx.com");
        user10.setUsername("madisonlopez");
        user10.setRole("buyer");
//        user10.setPassword("LopezMadi50n@");
        user10.setPassword(PasswordEncoder.encode("LopezMadi50n@"));
        userService.save(user10);

        defaultUserData.addAll(userService.findAll());
    }

    private void populateProductTable() {
        Product product1 = new Product();
        product1.setName("Book");
        product1.setPrice("500");
        product1.setQuantity(10);
        product1.setSeller(defaultUserData.get(0));
        productService.save(product1);

        Product product2 = new Product();
        product2.setName("Phone");
        product2.setPrice("800");
        product2.setQuantity(5);
        product2.setSeller(defaultUserData.get(1));
        productService.save(product2);

        Product product3 = new Product();
        product3.setName("Laptop");
        product3.setPrice("1200");
        product3.setQuantity(3);
        product3.setSeller(defaultUserData.get(2));
        productService.save(product3);

        Product product4 = new Product();
        product4.setName("Camera");
        product4.setPrice("600");
        product4.setQuantity(8);
        product4.setSeller(defaultUserData.get(3));
        productService.save(product4);

        Product product5 = new Product();
        product5.setName("Smartwatch");
        product5.setPrice("300");
        product5.setQuantity(12);
        product5.setSeller(defaultUserData.get(1));
        productService.save(product5);

        defaultProductData.addAll(productService.findAll());
    }

    private void populateCartTable() {
        Cart cart1 = new Cart();
        cart1.setQuantity(2);
        cart1.setBuyer(defaultUserData.get(5));
        cart1.setProduct(defaultProductData.get(0));
        cartService.save(cart1);

        Cart cart2 = new Cart();
        cart2.setQuantity(1);
        cart2.setBuyer(defaultUserData.get(6));
        cart2.setProduct(defaultProductData.get(1));
        cartService.save(cart2);

        Cart cart3 = new Cart();
        cart3.setQuantity(3);
        cart3.setBuyer(defaultUserData.get(7));
        cart3.setProduct(defaultProductData.get(2));
        cartService.save(cart3);

        Cart cart4 = new Cart();
        cart4.setQuantity(2);
        cart4.setBuyer(defaultUserData.get(8));
        cart4.setProduct(defaultProductData.get(3));
        cartService.save(cart4);

        Cart cart5 = new Cart();
        cart5.setQuantity(1);
        cart5.setBuyer(defaultUserData.get(8));
        cart5.setProduct(defaultProductData.get(4));
        cartService.save(cart5);
    }


}

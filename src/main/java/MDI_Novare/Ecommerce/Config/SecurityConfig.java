package MDI_Novare.Ecommerce.Config;

import MDI_Novare.Ecommerce.Service.UserDetailServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private UserDetailServiceImplementation userDetailServiceImplementation;

        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
            http.authorizeHttpRequests()
                    .requestMatchers(HttpMethod.GET, "/product/update").hasRole("SELLER")
                    .requestMatchers(HttpMethod.GET, "/product/add").hasRole("SELLER")
                    .requestMatchers(HttpMethod.GET, "/product/save").hasRole("SELLER")
                    .requestMatchers(HttpMethod.GET, "/cart").hasRole("BUYER")
                    .requestMatchers("/css/**").permitAll()
                    .requestMatchers("/j/**").permitAll()
                    .anyRequest().permitAll()
                    .and()
                    .formLogin().loginPage("/login").defaultSuccessUrl("/home", true)
                    .permitAll()
                    .and()
                    .logout().logoutSuccessUrl("/login?logout=success")
                    .and()
                    .csrf().disable();
            return http.build();
        }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailServiceImplementation).passwordEncoder(new BCryptPasswordEncoder());
    }
}

package MDI_Novare.Ecommerce.Config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Base64;

public class PasswordEncoder {
    public static String encode(String password){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
//        String hashedPassword = passwordEncoder.encode(password);
//        return Base64.getEncoder().encodeToString(hashedPassword.getBytes());
    }
}
